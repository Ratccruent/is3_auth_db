-- ****** Object: Table DEV9.ASPNETUSERCLAIMS Script Date: 6/29/2017 2:59:38 PM ******
  CREATE TABLE "ASPNETUSERCLAIMS" 
   (	"ID" NUMBER(10,0) NOT NULL ENABLE,
	"USERID" NVARCHAR2(128) NOT NULL ENABLE,
	"CLAIMTYPE" NCLOB,
	"CLAIMVALUE" NCLOB,
	CONSTRAINT "PK_ASPNETUSERCLAIMS" PRIMARY KEY ("ID") ENABLE
   );