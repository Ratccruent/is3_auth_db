-- ****** Object: Table DEV9.CLIENTIDPRESTRICTIONS Script Date: 6/29/2017 2:59:38 PM ******
  CREATE TABLE "CLIENTIDPRESTRICTIONS" 
   (	"ID" NUMBER(10,0) NOT NULL ENABLE,
	"PROVIDER" NVARCHAR2(200) NOT NULL ENABLE,
	"CLIENT_ID" NUMBER(10,0) NOT NULL ENABLE,
	CONSTRAINT "PK_CLIENTIDPRESTRICTIONS" PRIMARY KEY ("ID") ENABLE
   );