-- ****** Object: Table DEV9.CLIENTS Script Date: 6/29/2017 2:59:38 PM ******
  CREATE TABLE "CLIENTS" 
   (	"ID" NUMBER(10,0) NOT NULL ENABLE,
	"ENABLED" NUMBER(1,0) NOT NULL ENABLE,
	"CLIENTID" NVARCHAR2(200) NOT NULL ENABLE,
	"CLIENTNAME" NVARCHAR2(200) NOT NULL ENABLE,
	"CLIENTURI" NVARCHAR2(2000),
	"LOGOURI" NCLOB,
	"REQUIRECONSENT" NUMBER(1,0) NOT NULL ENABLE,
	"ALLOWREMEMBERCONSENT" NUMBER(1,0) NOT NULL ENABLE,
	"ALLOWACCESSTOKENSVIABROWSER" NUMBER(1,0) NOT NULL ENABLE,
	"FLOW" NUMBER(10,0) NOT NULL ENABLE,
	"ALLOWCLIENTCREDENTIALSONLY" NUMBER(1,0) NOT NULL ENABLE,
	"LOGOUTURI" NCLOB,
	"LOGOUTSESSIONREQUIRED" NUMBER(1,0) NOT NULL ENABLE,
	"REQUIRESIGNOUTPROMPT" NUMBER(1,0) NOT NULL ENABLE,
	"ALLOWACCESSTOALLSCOPES" NUMBER(1,0) NOT NULL ENABLE,
	"IDENTITYTOKENLIFETIME" NUMBER(10,0) NOT NULL ENABLE,
	"ACCESSTOKENLIFETIME" NUMBER(10,0) NOT NULL ENABLE,
	"AUTHORIZATIONCODELIFETIME" NUMBER(10,0) NOT NULL ENABLE,
	"ABSOLUTEREFRESHTOKENLIFETIME" NUMBER(10,0) NOT NULL ENABLE,
	"SLIDINGREFRESHTOKENLIFETIME" NUMBER(10,0) NOT NULL ENABLE,
	"REFRESHTOKENUSAGE" NUMBER(10,0) NOT NULL ENABLE,
	"UPDATEACCESSTOKENONREFRESH" NUMBER(1,0) NOT NULL ENABLE,
	"REFRESHTOKENEXPIRATION" NUMBER(10,0) NOT NULL ENABLE,
	"ACCESSTOKENTYPE" NUMBER(10,0) NOT NULL ENABLE,
	"ENABLELOCALLOGIN" NUMBER(1,0) NOT NULL ENABLE,
	"INCLUDEJWTID" NUMBER(1,0) NOT NULL ENABLE,
	"ALWAYSSENDCLIENTCLAIMS" NUMBER(1,0) NOT NULL ENABLE,
	"PREFIXCLIENTCLAIMS" NUMBER(1,0) NOT NULL ENABLE,
	"ALLOWACCESSTOALLGRANTTYPES" NUMBER(1,0) NOT NULL ENABLE,
	CONSTRAINT "PK_CLIENTS" PRIMARY KEY ("ID") ENABLE
   );