-- ****** Object: Table DEV9.TOKENS Script Date: 6/29/2017 2:59:38 PM ******
  CREATE TABLE "TOKENS" 
   (	"KEY" NVARCHAR2(128) NOT NULL ENABLE,
	"TOKENTYPE" NUMBER(5,0) NOT NULL ENABLE,
	"SUBJECTID" NVARCHAR2(200),
	"CLIENTID" NVARCHAR2(200) NOT NULL ENABLE,
	"JSONCODE" NCLOB NOT NULL ENABLE,
	"EXPIRY" TIMESTAMP(6) WITH TIME ZONE NOT NULL ENABLE,
	CONSTRAINT "PK_TOKENS" PRIMARY KEY ("KEY","TOKENTYPE") ENABLE
   );