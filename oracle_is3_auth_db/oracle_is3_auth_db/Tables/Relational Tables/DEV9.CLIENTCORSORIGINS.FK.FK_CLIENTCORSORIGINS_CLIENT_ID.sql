-- ****** Object: Constraint DEV9.FK_CLIENTCORSORIGINS_CLIENT_ID Script Date: 6/29/2017 2:59:38 PM ******
ALTER TABLE "CLIENTCORSORIGINS" ADD CONSTRAINT "FK_CLIENTCORSORIGINS_CLIENT_ID" FOREIGN KEY ("CLIENT_ID") REFERENCES "CLIENTS"("ID") ON DELETE CASCADE ENABLE;
