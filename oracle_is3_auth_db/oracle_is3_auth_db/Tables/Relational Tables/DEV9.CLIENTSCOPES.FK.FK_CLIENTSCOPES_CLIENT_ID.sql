-- ****** Object: Constraint DEV9.FK_CLIENTSCOPES_CLIENT_ID Script Date: 6/29/2017 2:59:38 PM ******
ALTER TABLE "CLIENTSCOPES" ADD CONSTRAINT "FK_CLIENTSCOPES_CLIENT_ID" FOREIGN KEY ("CLIENT_ID") REFERENCES "CLIENTS"("ID") ON DELETE CASCADE ENABLE;
